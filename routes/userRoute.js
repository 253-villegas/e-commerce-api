const express = require("express");
const router = express.Router();
const  userController = require("../controllers/userController");
const auth = require("../auth");
const Product = require("../models/Product");
const Order = require('../models/Order');

// ========== Check email list ==========
router.post("/checkEmail", (req,res) => {

	userController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController)).catch(err => res.send(err));
});

// ========== Route for user registration ==========
router.post("/register", (req, res) => {

	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController)).catch(err => res.send(err));
			
});

// ========== Route for login User ==========
router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController)).catch(err => res.send(err));
});

// ========== Route for login admin ==========
router.post("/admin", (req, res) => {
	userController.loginAdmin(req.body).then(resultFromController => res.send(resultFromController)).catch(err => res.send(err));
});

// ==========  Route for getting all users details ==========
router.get("/all", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization);
	if(userData.isAdmin){
		userController.getAllUsers(req.body).then(resultFromController => res.send(resultFromController)).catch(err => res.send(err));
	} else {
		res.send(false);
	}

});

// ========== Route for retrieving user details ==========

  
router.get("/details", (req, res) => {

	const userData = auth.decode(req.headers.authorization);

	userController.getProfile({ userId : userData.id }).then(resultFromController => res.send(resultFromController)).catch(err => res.send(err))
});


// ========== Checkout Orders ==========
router.post("/checkout", auth.verify, (req, res) => {
  const userData = auth.decode(req.headers.authorization);

  let data = {
	userId : userData.id,
	isAdmin : userData.isAdmin,
	productId : req.body.productId,
	quantity : req.body.quantity
  }

    if(!userData.isAdmin){

    	userController.checkout(data).then(resultFromController => res.send(resultFromController)).catch(err => res.send(err))
        
    } else {

        res.send("Change to user account")
    }
});

// ========== Route for getting all orders ==========
router.get("/orders", auth.verify, (req, res) => {
  const userData = auth.decode(req.headers.authorization);

	  if (!userData.isAdmin) {
	    res.send("Unauthorized Access");
	  } else {
	    userController.getAllOrders()
	      .then(resultFromController => res.send(resultFromController))
	      .catch(err => res.send(err));
	  }
});

// ========== Route for getting all orders ==========
router.get("/cartAllOrders", auth.verify, (req, res) => {
  const userData = auth.decode(req.headers.authorization);

	  if (!userData.isAdmin) {
	    res.send("Unauthorized Access");
	  } else {
	    userController.getAllOrders()
	      .then(resultFromController => res.send(resultFromController))
	      .catch(err => res.send(err));
	  }
});

// ========== Route for retrieving user orders ==========
router.get('/myOrders', auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization)

	if (!userData.isAdmin) {
		userController.myOrders(userData.id).then(result => res.send(result))
		.catch(err => res.send(err))
	}
	
})



// ========== Route for setting an admin ==========
router.put("/:userId/setAsAdmin", auth.verify, (req, res) => { //need middleware

	const userData = auth.decode(req.headers.authorization);

	if(userData.isAdmin){
		userController.setAsAdmin(req.params, req.body).then(resultFromController => res.send(resultFromController))
		.catch(err => res.send(err));
	} else {
		res.send(false);
	}

});

// ========== Add to cart ==========
// router.post("/addToCart", auth.verify, async (req, res) => {
//   const userData = auth.decode(req.headers.authorization);

//   const data = {
//     userId: userData.id,
//     isAdmin: userData.isAdmin,
//     productId: req.body.productId,
//     quantity: req.body.quantity,
//   };

//   if (!data.isAdmin) {
//     try {
//       const isProductAdded = await userController.addToCart(data);
//       if (isProductAdded) {
//         res.status(200).json({
//           message: "Product added to cart successfully",
//         });
//       } else {
//         res.status(500).json({
//           message: "Failed to add product to cart",
//         });
//       }
//     } catch (err) {
//       res.status(500).json({
//         message: "Failed to add product to cart",
//       });
//     }
//   } else {
//     res.status(403).json({
//       message: "Admin not allowed to enroll",
//     });
//   }
// });

// ========== Change quantity ==========
// router.put("/changeQuantity", auth.verify, function(req, res) {
//   const userData = auth.decode(req.headers.authorization);
//   console.log(req.body);

//   if (userData.isAdmin) {
//     userController.changeQuantity(req.body)
//       .then(resultFromController => res.send(resultFromController))
//       .catch(err => res.send(err));
//   } else {
//     res.send(false);
//   }
// });

// ========== Route for checkout from cart ==========
// router.post("/checkoutFromCart", auth.verify, async (req, res) => {
//   userController.checkoutFromCart().then(resultFromController => res.send(resultFromController)).catch(err => res.send(err));
// });













// Allows us to export the "router" object that will be accessed in our "index.js" file
module.exports = router;