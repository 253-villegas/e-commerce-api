const Product = require("../models/Product");
const User = require("../models/User");

// ========== Create product ==========
module.exports.addProduct = (body) => {
	let newProduct = new Product({
		name: body.name,
		description: body.description,
		price: body.price,
		isActive: body.isActive
	})

	return newProduct.save().then(result => result)
		.catch(err => err)
}


// ========== Getting all products ==========
module.exports.getAllProducts = () => {
	return Product.find({}).then(result => result)
		.catch(err => err);
};

// ========== Getting all active pruducts ==========
module.exports.getAllActiveProducts = () => {

	return Product.find({ isActive : true }).then(result => result)
		.catch(err => err);
};

// ========== Getting single product ==========
module.exports.getProduct = (reqParams) => {

	return Product.findById(reqParams.productId).then(result => {
		return result;
	}).catch(err => err);

};

// ========== Update products ==========
module.exports.updateProduct = (reqParams, reqBody) => {

	let updateProduct = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price,
		isActive: reqBody.isActive
	};

	return Product.findByIdAndUpdate(reqParams.productId, updateProduct)
	.then(result => result).catch(err => err);
};

// ========== Archive product ==========
module.exports.archiveProduct = (reqParams, reqBody) => {

	let updateActiveField = {
		isActive : reqBody.isActive	
	};

	return Product.findByIdAndUpdate(reqParams.productId, updateActiveField).then(result => result).catch(err => err);
};

// ========== Activate products ==========
module.exports.ativateProduct = (reqParams, reqBody) => {

	let updateActiveField = {
		isActive : reqBody.isActive	
	};

	return Product.findByIdAndUpdate(reqParams.productId, updateActiveField).then(result => result).catch(err => err);


};
