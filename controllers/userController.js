const bcrypt = require("bcrypt");
const auth = require("../auth");
const User = require("../models/User");
const Product = require("../models/Product");
const Order = require('../models/Order');

// ========== Check email list ==========
module.exports.checkEmailExists = (reqBody) => {

	// The result is sent back to the frontend via the "then" method found in the route file
	return User.find({ email : reqBody.email }).then(result => {

		// The "find" method returns a record if a match is found
		if(result.length > 0) {

			return true;
		// No duplicate email found
		// The user is not yet registered in the database
		} else {

			return false;
		}
	}).catch(err => err);
};


// ========== User registration ==========
module.exports.registerUser = (reqBody) => {
    let newUser = new User({
        firstName : reqBody.firstName,
        lastName : reqBody.lastName,
        email : reqBody.email,
        mobileNo : reqBody.mobileNo,
        // 10 is the value provided as the number of "salt" rounds that the bcrypt algorithm will run in order to encrypt the password
        password : bcrypt.hashSync(reqBody.password, 10)
    });

    return newUser.save().then(user => {

        if(user){

            return true;
            
        } else {

            return false;
        }
    }).catch(err => err)
};

// module.exports.registerUser = (reqBody) => {


// 	return User.find({email : reqBody.email}).then(result => {

// 		if(result.length > 0) {

// 			return false;

// 		} else{

// 			let newUser = new User({
// 			    firstName : reqBody.firstName,
// 			    lastName : reqBody.lastName,
// 			    email : reqBody.email,
// 				mobileNo : reqBody.mobileNo,
// 			    password : bcrypt.hashSync(reqBody.password, 10),

// 			});

// 			return newUser.save().then(user => {

// 			    if(user){

// 			        return  true;
			        
// 			    } else {

// 			        return "Try Again!";
// 			    }
// 			}).catch(err => err)
// 		}
// 	}).catch(err => err);
// };

// ========== User login ==========
module.exports.loginUser = (reqBody) => {

	return User.findOne({ email : reqBody.email }).then(result => {

		if(result == null){

			return true;

		} else {
			
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);
			if(isPasswordCorrect){

				return { access : auth.createAccessToken(result) }

			} else {

				return false;
			}
		}
	}).catch(err => err);
}

// ========== Admin login ==========
module.exports.loginAdmin = (reqBody) => {

	return User.findOne({ email : reqBody.email }).then(result => {

		if(result == null){

			return "Incorrect Email!";

		} else {
			
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);
			if(isPasswordCorrect){

				return { access : auth.createAccessToken(result) }

			} else {

				return "Incorrect Password!";
			}
		}
	}).catch(err => err);
}

// ========== Get all users ==========
module.exports.getAllUsers = () => {
	return User.find({}).then(result => result)
		.catch(err => err);
};

// ========== Order Checkout ==========
// module.exports.checkout = async (data) => {

// 	// Add the course ID in the enrollments array of the user
// 	// Creates an "isUserUpdated" variable and returns true upon successful update otherwise false
// 	// Using the "await" keyword will allow the enroll method to complete updating the user before returning a response back to the frontend
// 	let isUserUpdated = await User.findById(data.userId)
// 		.then(user => {

// 			// Adds the courseId in the user's enrollments array
// 			user.orders.push({ productId : data.productId });

// 			// Saves the updated user information in the database
// 			return user.save().then(user => true)
// 				.catch(err => err)
// 		});

// 	// Add the user ID in the enrollees array of the course
// 	// Using the "await" keyword will allow the enroll method to complete updating the course before returning a response back to the frontend
// 	let isProductUpdated = await Product.findById(data.productId)
// 		.then(product => {

// 			// Adds the userId in the course's enrollees array
// 			product.purchased.push({ userId : data.userId });

// 			// Saves the updated course information in the database
// 			return product.save().then(product => true)
// 				.catch(err => false);

// 		});

// 	// Condition that will check if the user and course documents have been updated
// 	// User enrollment successful
// 	if(isUserUpdated && isProductUpdated){
// 		return true;

// 	// User enrollment failure
// 	} else {
// 		return false
// 	}

// }
module.exports.checkout = async (data) => {
	try {
	  const product = await Product.findById(data.productId);
	  if (!product) {
		throw new Error('Product not found');
	  }
  
	  // Create a new order object with user ID, product ID, and quantity
	  const newOrder = new Order({
		userId: data.userId,
		products: [
		  {
			productId: data.productId,
			quantity: data.quantity,
		  },
		],
		totalAmount: product.price * data.quantity,
	  });
  
	  // Save the new order to the database
	  const savedOrder = await newOrder.save();
  
	  // Update the product's quantity in the database
	  
  
	  return savedOrder;
	} catch (err) {
	  console.error(err);
	  return err;
	}
  };

// ========== Get user details ==========
module.exports.getProfile = (data) => {

	return User.findById(data.userId).then(result => {

		// Changes the value of the user's password to an empty string when returned to the frontend
		// Not doing so will expose the user's password which will also not be needed in other parts of our application
		// Unlike in the "register" method, we do not need to call the mongoose "save" method on the model because we will not be changing the password of the user in the database but only the information that we will be sending back to the frontend application
		result.password = "";

		// Returns the user information with the password as an empty string
		return result;

	});

};



// ==========  Retrieve all orders ==========
module.exports.getAllOrders = () => {return Order.find().then(result => result).catch(err => err);
};
// ==========  Retrieve all orders from cart ==========
module.exports.cartAllOrders = () => {return User.find({ userId: cart}).then(result => result).catch(err => err);
};



// ========== Retrieve user order ==========
module.exports.myOrders = async (id) => {

		const allMyOrders = await Order.find({ userId: id }).then(result => result).catch(err => err)
		return allMyOrders;

}

// ========== Set as admin ==========
module.exports.setAsAdmin = (reqParams, reqBody) => {

	let updateAdminField = {
		isAdmin : reqBody.isAdmin	
	};

	return User.findByIdAndUpdate(reqParams.userId, updateAdminField).then(result => result).catch(err => err);


};

// ========== Add to cart ==========
module.exports.addToCart = async (data) => {
  let isProductAdded = await User.findById(data.userId).then((user) => {
    return Product.findById(data.productId).then((product) => {
      if (!product) {
        return "Product not found";
      }

      const subTotal = product.price * data.quantity;
      user.cart.push({
        productId: data.productId,
        quantity: data.quantity,
        subTotal: subTotal,
      });
      return user.save().then((user) => true).catch((err) => err);
    });
  });

  if (isProductAdded) {
    return true;
  } else {
    return false;
  }
};

// ========== Change Qty ==========
module.exports.changeQuantity = (productId, quantity) => {
  return user.findOneAndUpdate(
    { "cart.productId": productId },
    { $set: { "cart.$.quantity": quantity } },
    { new: true }
  )
  .then(result => result)
  .catch(err => err);
};
// ========== Checout from cart ==========
module.exports.checkoutFromCart = async (reqBody, data) => {

  try {

  	  // Get the user ID from the decoded authorization token
  	  const userData = auth.decode(req.headers.authorization);
  	  const userId = userData.id;

  	  // Get the user's cart
  	  const user = await userController.getUser(userId);
  	  const cart = user.cart;

  	  // Create an array of promises to get the product details for each item in the cart
  	  const promises = cart.map(async (item) => {
  	    const product = await Product.findById(item.productId);
  	    return {
  	      productId: item.productId,
  	      name: product.name,
  	      price: product.price,
  	      quantity: item.quantity,
  	    };
  	  });

  	  // Wait for all the promises to resolve
  	  const products = await Promise.all(promises);

  	  // Calculate the total amount for the order
  	  const totalAmount = products.reduce(
  	    (total, product) => total + product.price * product.quantity,
  	    0
  	  );

  	  // Create a new order
  	  const order = new Order({
  	    userId: userId,
  	    products: products,
  	    totalAmount: totalAmount,
  	  });

  	  // Save the order to the database
  	  const savedOrder = await order.save();

  	  // Empty the user's cart
  	  user.cart = [];
  	  await user.save();

  	  res.send(savedOrder);
  	} catch (err) {
  	  console.log(err);
  	  res.status(500).send("Internal Server Error");
    // Get the product ID and price
    const product = await Product.findById(reqBody.productId);
    const productId = product._id;
    const price = product.price;

    // Create a new order
    const order = new Order({
      userId: data,
      products: [{ productId: productId, quantity: reqBody.quantity }],
      totalAmount: price * reqBody.quantity,
    });

    // Save the order to the database
    const savedOrder = await order.save();

    return savedOrder;
  }
};